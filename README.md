### Setup the project

- Clone "hrms/core" git repository in the same directory as this project folder

```
dev/
 +- core/
 +- <template>/
```

```
$ git clone https://tools.digitalventures.co.th:8443/hrms/core.git
```

- Package dependencies

```
$ cd <template>
$ export GOPATH=`pwd`
$ export GO111MODULE=on
$ cd src/api
$ go mod download
```

- Run the main function

```
$ export ENVIRONMENT_NAME=develop
$ go run main.go
```

## Command

- How to run with easy mode

  - `chmod +x run_helper.sh` - once time
  - start container `./run_helper.sh`
  - start app with `/root/entrypoint.sh` - when in docker container
  - open [http://localhost:9081](http://localhost:9081) on your browser

- Generate Swagger Doc [swaggo](https://github.com/swaggo/swag)
  - `swag i` - when in docker container
  - Open and view swagger definition at http://localhost:9081/auth/docs/swagger.json
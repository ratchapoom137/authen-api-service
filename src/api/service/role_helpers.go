package service

import (
	"api/base"
	"api/model"

	"github.com/jinzhu/gorm"
)

// Filter implementation
type roleSearchFilter struct {
	id *int
	name *string
}

func (usf roleSearchFilter) Bind(db *gorm.DB) *gorm.DB {
	if usf.id != nil {
		db = db.Where("id = (?)", usf.id)
	}
	if usf.name != nil {
		db = db.Where("name = (?)", usf.name)
	}
	return db
}

// Search implementation
func searchRole(options ...searchOption) (ret searchResult, err error) {
	var users []model.Role
	var count int
	app := base.GetApplication()

	db := app.DB

	db = db.Model(&model.Role{})

	for _, option := range options {
		db = option(db)
	}

	if result := db.Preload("Permission").Find(&users); result.Error != nil {
		app.Logger.Error(result.Error)

		err = result.Error
		return
	}

	db.Count(&count)

	ret.result = users
	ret.total = count

	return
}

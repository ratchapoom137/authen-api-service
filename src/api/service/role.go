package service

import (
	"api/constant"
	"api/model"

	"dv.co.th/hrms/core/rest"
)

// GetRole -
func GetRole(filter map[string]interface{}) (roles interface{}, ok bool, result rest.APIResultStatus) {
	var roleFilter roleSearchFilter
	if name, ok := filter["name"].(string); ok && name != "" {
		roleFilter.name = &name
	}

	output, _ := searchRole(withFilter(roleFilter))

	if output.total > 0 {
		ok = true
		roles = output.result.([]model.Role)

		return
	}

	result.Message = constant.ReturnConst["RoleNotFoundConst"].Message
	result.Code = constant.ReturnConst["RoleNotFoundConst"].Code

	return
}
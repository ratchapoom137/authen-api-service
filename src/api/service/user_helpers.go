package service

import (
	"api/base"
	"api/model"
	"log"
	"time"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

// Filter implementation
type userSearchFilter struct {
	userID   *int
	username *string
}

type usernameSearchFilter struct {
	username string
	roleName string
	searchUsername string
}

func (usf userSearchFilter) Bind(db *gorm.DB) *gorm.DB {
	if usf.username != nil {
		db = db.Where("LOWER(username) = LOWER(?)", usf.username)
	}
	if usf.userID != nil {
		db = db.Where("id = (?)", usf.userID)
	}

	return db
}

func (usnf usernameSearchFilter) BindUsername(db *gorm.DB) *gorm.DB {

	app := base.GetApplication()
	subQueryRole :=  app.DB.Table(model.Role{}.TableName()).Select("id")

	var isRoleName bool

	if usnf.username != "" {
		db = db.Where("LOWER(username) = LOWER(?)", usnf.username)
	}
	if usnf.searchUsername != "" {
		db = db.Where("LOWER(username) LIKE LOWER(?)", "%"+usnf.searchUsername+"%")
	}
	if usnf.roleName != "" {
		isRoleName = true
		subQueryRole = subQueryRole.Where("name = ?", usnf.roleName)
	}

	if isRoleName {
		db = db.Where("role_id = (?)", subQueryRole.QueryExpr())
		app.Logger.Info(subQueryRole.QueryExpr())
	}

	return db
}

// Search implementation
func searchUser(options ...searchOption) (ret searchResult, err error) {
	var users []model.User
	var count int
	app := base.GetApplication()

	db := app.DB

	db = db.Model(&model.User{})

	for _, option := range options {
		db = option(db)
	}

	if result := db.Preload("Role.Permission").Order("id desc").Find(&users); result.Error != nil {
		app.Logger.Error(result.Error)

		err = result.Error
		return
	}

	db.Limit(-1).Offset(0).Count(&count)

	ret.result = users
	ret.total = count

	return
}

// Helper
func comparePasswords(hashedPwd string, plainPwdStr string) bool {
	if err := bcrypt.CompareHashAndPassword([]byte(hashedPwd), []byte(plainPwdStr)); err != nil {
		log.Println(err)
		return false
	}

	return true
}

func hashAndSalt(pwd string) string {
	// Return the users input as a byte slice which will save us
	// from having to do this conversion later on
	pwdByte := []byte(pwd)

	// Use GenerateFromPassword to hash & salt pwd.
	// MinCost is just an integer constant provided by the bcrypt
	// package along with DefaultCost & MaxCost.
	// The cost can be any value you want provided it isn't lower
	// than the MinCost (4)
	hash, err := bcrypt.GenerateFromPassword(pwdByte, bcrypt.MinCost)
	if err != nil {
		log.Println(err)
	}
	// GenerateFromPassword returns a byte slice so we need to
	// convert the bytes to a string and return it
	return string(hash)
}

func ifStr(condition bool, ifTrue string, ifFalse string) string {
	if condition {
		return ifTrue
	}
	return ifFalse
}

func ifInt(condition bool, ifTrue int, ifFalse int) int {
	if condition {
		return ifTrue
	}
	return ifFalse
}

func ifDate(condition bool, ifTrue time.Time, ifFalse time.Time) time.Time {
	if condition {
		return ifTrue
	}
	return ifFalse
}

package service

import (
	"api/base"
	"api/constant"
	"api/model"
	"errors"
	"strconv"

	"dv.co.th/hrms/core"
	"dv.co.th/hrms/core/rest"
	"encoding/json"
)

type UserIDList struct {
	UserIDList []int `json:"user_id_list"`
}

// CreateUser handle for user creation api
func CreateUser(username, password string, roleIDInput int) (user model.User, ok bool, result rest.APIResultStatus) {
	app := base.GetApplication()
	if username == "" {
		result.Message = "Blank username"
		result.Code = constant.ReturnConst["UserCreationFail"].Code

		return
	}

	password = hashAndSalt(password)

	user.Username = username
	user.Password = password

	roleFilter := roleSearchFilter{
		id: &roleIDInput,
	}

	retRole, retRoleErr := searchRole(withFilter(roleFilter))
	if retRoleErr != nil {
		return
	}

	var resultParsed []interface{}
	jsonData, _ := json.Marshal(retRole.result)
	json.Unmarshal(jsonData, &resultParsed)

	var roleParsed model.Role
	for _, item := range resultParsed {
		jsonData, _ := json.Marshal(item)
		json.Unmarshal(jsonData, &roleParsed)
	}
	user.RoleID = roleParsed.ID
	user.Role = &roleParsed

	filter := userSearchFilter{
		username: &username,
	}

	ret, _ := searchUser(withFilter(filter))

	if ret.total > 0 {
		result.Message = "Username already exists"
		result.Code = constant.ReturnConst["UserCreationFail"].Code

		return
	}

	if err := user.Create(); err != nil {
		app.Logger.Error(err.Error())
		result.Message = constant.ReturnConst["UserCreationFail"].Message
		result.Code = constant.ReturnConst["UserCreationFail"].Code

		return
	}

	ok = true

	result.Message = constant.ReturnConst["Success"].Message
	result.Code = constant.ReturnConst["Success"].Code

	return
}

// GetUserFromUserID -
func GetUserFromUserID(UserID int) (user interface{}, ok bool, result rest.APIResultStatus) {
	filter := userSearchFilter{
		userID: &UserID,
	}

	ret, _ := searchUser(withFilter(filter))

	if ret.total > 0 {
		userResult := ret.result.([]model.User)[0]

		ok = true
		user = userResult.GetOutput("user")
		return
	}

	result.Message = constant.ReturnConst["UserNotFoundConst"].Message
	result.Code = constant.ReturnConst["UserNotFoundConst"].Code

	return
}

// GetUserFromUserNameAndPassword -
func GetUserFromUserNameAndPassword(username, password string) (user interface{}, ok bool, result rest.APIResultStatus) {
	app := base.GetApplication()

	filter := userSearchFilter{
		username: &username,
	}

	ret, _ := searchUser(withFilter(filter))

	if ret.total > 0 {
		userResult := ret.result.([]model.User)[0]

		if comparePasswords(userResult.Password, password) == true {
			ok = true
			user = userResult.GetOutput("user")
			return
		}
		app.Logger.Info("Login fail. ", username)
	}

	// if username == "admin" {
	// 	ok = true
	// 	user = model.User{
	// 		ID: 1,
	// 	}

	// 	return
	// }

	// if username == "hr" {
	// 	ok = true
	// 	user = model.User{
	// 		ID: 2,
	// 	}
	// 	return
	// }

	result.Message = constant.ReturnConst["LoginFailConst"].Message
	result.Code = constant.ReturnConst["LoginFailConst"].Code

	return
}

//GetUserList --
func GetUserList(filter map[string]interface{}) (users interface{}, ok bool, total int) {

	var userFilter usernameSearchFilter
	limit, _ := strconv.Atoi(filter["limit"].(string))
	offset, _ := strconv.Atoi(filter["offset"].(string))

	if username, ok := filter["username"].(string); ok && username != "" {
		userFilter.username = username
	}
	if searchUsername, ok := filter["search_username"].(string); ok && searchUsername != "" {
		userFilter.searchUsername = searchUsername
	}
	if role_name, ok := filter["role_name"].(string); ok && role_name != "" {
		userFilter.roleName = role_name
	}

	res, _ := searchUser(withLimit(limit), withOffset(offset), withUsernameFilter(userFilter))

	if res.total >= 1 {
		users = res.result.([]model.User)
		total = res.total
		ok = true
		return
	}
	return

}

//DeleteUser --
func DeleteUser(userID int) error {
	app := core.GetApplication()
	db := app.DB
	var user model.User

	if userResult := db.First(&user, userID); userResult.Error != nil {
		return userResult.Error
	}
	if userResult := db.Delete(model.User{ID: userID}); userResult.Error != nil {
		return errors.New("Cannot delete user")
	}
	return nil
}

// GetUserFromUserID -
func GetUserByID(userID int) (userInfo model.User, err error) {
	app := core.GetApplication()
	db := app.DB

	if userResult := db.First(&userInfo, userID); userResult.Error != nil {
		err = userResult.Error
		return
	}
	return
}

//EditUserInfo --
func EditUserInfo(userID int, newUserInfo model.User) error {
	oldUserInfo, err := GetUserByID(userID)

	if err != nil {
		return err
	}
	
	// Update role ID
	oldUserInfo.RoleID = ifInt(newUserInfo.RoleID != 0, newUserInfo.RoleID, oldUserInfo.RoleID)

	// Update username and password will general empty hash
	if newUserInfo.Username != "" {
		oldUserInfo.Username = ifStr(newUserInfo.Username != "", newUserInfo.Username, oldUserInfo.Username)
		if newUserInfo.Username != oldUserInfo.Username {
			oldUserInfo.Password = hashAndSalt(ifStr(newUserInfo.Password != "", newUserInfo.Password, oldUserInfo.Password))
		}
	}

	// Update password
	if newUserInfo.Password != "" {
		password := newUserInfo.Password
		oldUserInfo.Password = hashAndSalt(password)
	}

	err = oldUserInfo.Edit()
	if err != nil {
		return errors.New("Cannot edit user information")
	}

	return nil
}

// CreatePasswordSession --
func CreatePasswordSession(passwordSession model.PasswordSession) (passwordID int, err error) {

	passwordID, err = passwordSession.Create()
	if err != nil {
		return
	}
	return
}

// GetPasswordSession -- 
func GetPasswordSessionByID(passwordSessionID int) (passwordSession model.PasswordSession, err error) {
	app := core.GetApplication()
	db := app.DB

	if passwordSessionResult := db.First(&passwordSession, passwordSessionID); passwordSessionResult.Error != nil {
		err = passwordSessionResult.Error
		return
	}
	return
}

// GetPasswordSessionList -- 
func GetPasswordSessionList(filter map[string]interface{}) (passwordSessions []model.PasswordSession, total int, err error) {
	app := core.GetApplication()
	db := app.DB

	limit, _ := strconv.Atoi(filter["limit"].(string))
	offset, _ := strconv.Atoi(filter["offset"].(string))

	if secret, ok := filter["secret"].(string); ok && secret != "" {
		db = db.Where("secret = ? ", secret)
	}
	if user_id, err := strconv.Atoi(filter["user_id"].(string)); err == nil {
		db = db.Where("user_id = (?)", user_id)
	}

	if result := db.Offset(offset).Limit(limit).Find(&passwordSessions); result.Error != nil {
		err = errors.New("password session not found")
		return
	}
	db.Model(model.PasswordSession{}).Count(&total)

	return

}

// DeletePasswordSession --
func DeletePasswordSession(passwordSessionID int) error {
	app := core.GetApplication()
	db := app.DB
	var passwordSession model.PasswordSession

	if passwordSessionResult := db.First(&passwordSession, passwordSessionID); passwordSessionResult.Error != nil {
		return passwordSessionResult.Error
	}
	if passwordSessionResult := db.Delete(model.PasswordSession{ID: passwordSessionID}); passwordSessionResult.Error != nil {
		return errors.New("Cannot delete password session")
	}
	return nil
}
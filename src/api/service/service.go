package service

import "github.com/jinzhu/gorm"

type searchResult struct {
	total  int
	result interface{}
}
type searchOption func(db *gorm.DB) *gorm.DB

type searchFilterInterface interface {
	Bind(*gorm.DB) *gorm.DB
}

type searchUserNameFilterInterface interface {
	BindUsername(*gorm.DB) *gorm.DB
}

func withFilter(filter searchFilterInterface) searchOption {
	return func(db *gorm.DB) *gorm.DB {
		db = filter.Bind(db)

		return db
	}
}

func withUsernameFilter(filter searchUserNameFilterInterface) searchOption {
	return func(db *gorm.DB) *gorm.DB {
		db = filter.BindUsername(db)

		return db
	}
}

func withLimit(limit int) searchOption {
	return func(db *gorm.DB) *gorm.DB {
		db = db.Limit(limit)
		return db
	}
}

func withOffset(offset int) searchOption {
	return func(db *gorm.DB) *gorm.DB {
		db = db.Offset(offset)
		return db
	}
}

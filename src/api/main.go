package main

import (
	"api/migration"
	"api/router"

	"dv.co.th/hrms/core"
	"github.com/kataras/iris"
)

// @title Auth MS
// @version 0.1.0
// @description Auth Microservice.

// @host localhost:9081
// @BasePath /auth

// @contact.name Jeurboy
// @contact.url http://www.swagger.io/support
// @contact.email pornprasith.m@dv.co.th

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

func main() {
	app := core.GetApplication()

	app.Logger.Info("Main - ", *app)

	irisApp := router.New()

	irisApp.Get("/migrate", func(ctx iris.Context) {
		migration.Execute()

		ctx.JSON(iris.Map{
			// "message": core.GetApplication().Env.JwtPublicKeyPath,
			"message": "Done",
		})
	})

	// listen and serve on http://0.0.0.0:8080.
	irisApp.Run(iris.Addr(":8080"))
}

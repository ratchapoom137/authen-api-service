package router

import (
	"api/base"
	restinterface "api/interface/rest"

	"github.com/iris-contrib/middleware/cors"
	"github.com/kataras/iris"
)

// RestRouter -
func RestRouter(irisApp *iris.Application) *iris.Application {
	app := base.GetApplication()
	crs := cors.New(cors.Options{
		AllowedMethods:   []string{"HEAD", "GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"*"},
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
	})

	cors := irisApp.Party(app.Env.UriBasePath+"/", crs).AllowMethods(iris.MethodOptions)

	cors.StaticWeb("/docs", "./docs")

	public := cors.Party("/login")
	{
		public.Handle("POST", "/", restinterface.Login)
	}

	user := cors.Party("/user")
	{
		user.Handle("POST", "/", restinterface.UserCreate)
		user.Handle("GET", "/{UserID:int}", restinterface.GetUserByID)
	}

	cors.Handle("GET", "/users", restinterface.GetUsers)
	cors.Handle("DELETE", "/user/{id: int}", restinterface.DeleteUser)
	cors.Handle("PUT", "/user/{id: int}", restinterface.PutUser)

	cors.Handle("GET", "/roles", restinterface.GetRoles)
	cors.Handle("POST", "/password-session", restinterface.PostPasswordSession)
	cors.Handle("GET", "/password-session/{id: int}", restinterface.GetPasswordSession)
	cors.Handle("GET", "/password-sessions", restinterface.GetPasswordSessions)
	cors.Handle("DELETE", "/password-session/{id: int}", restinterface.DeletePasswordSession)


	return irisApp
}

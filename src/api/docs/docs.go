// GENERATED BY THE COMMAND ABOVE; DO NOT EDIT
// This file was generated by swaggo/swag at
// 2019-09-26 09:12:51.001696723 +0000 UTC m=+0.314004324

package docs

import (
	"bytes"
	"encoding/json"
	"strings"

	"github.com/alecthomas/template"
	"github.com/swaggo/swag"
)

var doc = `{
    "schemes": {{ marshal .Schemes }},
    "swagger": "2.0",
    "info": {
        "description": "{{.Description}}",
        "title": "{{.Title}}",
        "contact": {
            "name": "Jeurboy",
            "url": "http://www.swagger.io/support",
            "email": "pornprasith.m@dv.co.th"
        },
        "license": {
            "name": "Apache 2.0",
            "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
        },
        "version": "{{.Version}}"
    },
    "host": "{{.Host}}",
    "basePath": "{{.BasePath}}",
    "paths": {
        "/login": {
            "post": {
                "consumes": [
                    "multipart/form-data",
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "public:user"
                ],
                "summary": "Get the latest person properties of a specified personn",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Username[admin|hr]",
                        "name": "username",
                        "in": "formData",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "Password",
                        "name": "password",
                        "in": "formData",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/restinterface.APIResult"
                        }
                    },
                    "401": {
                        "description": "Unauthorized",
                        "schema": {
                            "$ref": "#/definitions/restinterface.APIResult"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/restinterface.APIResult"
                        }
                    }
                }
            }
        },
        "/role": {
            "get": {
                "consumes": [
                    "multipart/form-data",
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "public:user"
                ],
                "summary": "Get the latest person properties of a specified personn",
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/restinterface.APIResult"
                        }
                    },
                    "401": {
                        "description": "Unauthorized",
                        "schema": {
                            "$ref": "#/definitions/restinterface.APIResult"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/restinterface.APIResult"
                        }
                    }
                }
            }
        },
        "/user": {
            "post": {
                "consumes": [
                    "multipart/form-data",
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "public:user"
                ],
                "summary": "Get the latest person properties of a specified personn",
                "parameters": [
                    {
                        "description": "UserCreateInput",
                        "name": "user",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/restinterface.UserCreateInput"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/restinterface.APIResult"
                        }
                    },
                    "401": {
                        "description": "Unauthorized",
                        "schema": {
                            "$ref": "#/definitions/restinterface.APIResult"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/restinterface.APIResult"
                        }
                    }
                }
            }
        },
        "/user/{id}": {
            "get": {
                "consumes": [
                    "multipart/form-data",
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "public:user"
                ],
                "summary": "Get user account from given id",
                "parameters": [
                    {
                        "type": "integer",
                        "description": "User ID",
                        "name": "userid",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/restinterface.APIResult"
                        }
                    },
                    "401": {
                        "description": "Unauthorized",
                        "schema": {
                            "$ref": "#/definitions/restinterface.APIResult"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/restinterface.APIResult"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "restinterface.APIResult": {
            "type": "object",
            "properties": {
                "data": {
                    "type": "object"
                },
                "status": {
                    "type": "object",
                    "$ref": "#/definitions/restinterface.APIResultStatus"
                },
                "total": {
                    "type": "integer"
                }
            }
        },
        "restinterface.APIResultStatus": {
            "type": "object",
            "properties": {
                "code": {
                    "type": "string"
                },
                "message": {
                    "type": "string"
                }
            }
        },
        "restinterface.UserCreateInput": {
            "type": "object",
            "properties": {
                "password": {
                    "type": "string"
                },
                "username": {
                    "type": "string"
                }
            }
        }
    }
}`

type swaggerInfo struct {
	Version     string
	Host        string
	BasePath    string
	Schemes     []string
	Title       string
	Description string
}

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo = swaggerInfo{
	Version:     "0.1.0",
	Host:        "localhost:9081",
	BasePath:    "/auth",
	Schemes:     []string{},
	Title:       "Auth MS",
	Description: "Auth Microservice.",
}

type s struct{}

func (s *s) ReadDoc() string {
	sInfo := SwaggerInfo
	sInfo.Description = strings.Replace(sInfo.Description, "\n", "\\n", -1)

	t, err := template.New("swagger_info").Funcs(template.FuncMap{
		"marshal": func(v interface{}) string {
			a, _ := json.Marshal(v)
			return string(a)
		},
	}).Parse(doc)
	if err != nil {
		return doc
	}

	var tpl bytes.Buffer
	if err := t.Execute(&tpl, sInfo); err != nil {
		return doc
	}

	return tpl.String()
}

func init() {
	swag.Register(swag.Name, &s{})
}

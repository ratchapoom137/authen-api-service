 package constant

// ReturnMap is a return schema for api rest
type ReturnMap struct {
	Code    string
	Message string
}

// ReturnConst -
var ReturnConst = map[string]ReturnMap{
	"Success": ReturnMap{
		Code:    "SUCCESS",
		Message: "Success",
	},
	"InvalidParamConst": ReturnMap{
		Code:    "INVALID_PARAM",
		Message: "Invalid param",
	},
	"UserCreationFail": ReturnMap{
		Code: "USER_CREATION_FAIL",
	},
	"UserNotFoundConst": ReturnMap{
		Code:    "USER_NOT_FOUND",
		Message: "User not found",
	},
	"RoleNotFoundConst": ReturnMap{
		Code:    "Role_NOT_FOUND",
		Message: "role not found",
	},
	"PasswordSessionNotFoundConst": ReturnMap{
		Code:    "PASSWORD_SESSION_NOT_FOUND",
		Message: "Password session not found",
	},
	"LoginFailConst": ReturnMap{
		Code:    "LOGIN_FALIED",
		Message: "Login fail",
	},
	"UserIDInvalidConst": ReturnMap{
		Code:    "USER_ID_INVALID",
		Message: "User ID invalid",
	},
	"ReadJsonFailedConst": ReturnMap{
		Code:    "READ_JSON_FAILED",
		Message: "Read json failed",
	},
	"SearchUserSuccessConst": ReturnMap{
		Code:    "SEARCH_USER_SUCCESS",
		Message: "Search user success",
	},
	"SearchUserListSuccessConst": ReturnMap{
		Code:    "SEARCH_USER_LIST_SUCCESS",
		Message: "Search user list success",
	},
	"SearchUserListFailedConst": ReturnMap{
		Code:    "SEARCH_USER_LIST_FAILED",
		Message: "Search user list failed",
	},
	"DeleteUserFailedConst": ReturnMap{
		Code:    "DELETE_USER_FAILED",
		Message: "Delete user failed",
	},
	"DeleteUserSuccessConst": ReturnMap{
		Code:    "DELETE_USER_SUCCESS",
		Message: "Delete user success",
	},
	"UpdateUserSuccessConst": ReturnMap{
		Code:    "UPDATE_USER_SUCCESS",
		Message: "Update user success",
	},
	"UpdateUserFailedConst": ReturnMap{
		Code:    "UPDATE_USER_FAILED",
		Message: "Update user failed",
	},
	"CreatePasswordSessionSuccessConst": ReturnMap{
		Code:    "CREATE_PASSWORD_SESSION_SUCCESS",
		Message: "Create password session success",
	},
	"CreatePasswordSessionFailedConst": ReturnMap{
		Code:    "CREATE_PASSWORD_SESSION_FAILED",
		Message: "Create password session failed",
	},
	"SearchPasswordSessionSuccessConst": ReturnMap{
		Code:    "SEARCH_PASSWORD_SESSION_SUCCESS",
		Message: "Search password session success",
	},
	"DeletePasswordSessionFailedConst": ReturnMap{
		Code:    "DELETE_PASSWORD_SESSION_FAILED",
		Message: "Delete password session failed",
	},
	"DeletePasswordSessionSuccessConst": ReturnMap{
		Code:    "DELETE_PASSWORD_SESSION_SUCCESS",
		Message: "Delete password session success",
	},

}

package model

import (
	"api/base"
	"time"
	"errors"

	
	"dv.co.th/hrms/core"
	validator "gopkg.in/go-playground/validator.v9"
	gopex "github.com/joaosilva2095/go-pex"
)

// User -
type User struct {
	ID       int    `json:"id" gorm:"column:id;type:int unsigned auto_increment;primary_key;unique;" pex:"user:r"`
	Username string `json:"username,omitempty" gorm:"column:username;not null;unique;" pex:"user:r"`
	Password string `json:"password" gorm:"column:password;not null" pex:"user:"`
	RoleID   int    `json:"role_id,omitempty" gorm:"column:role_id;not null" pex:"user:"`

	// Association
	Role *Role `json:"role,omitempty" gorm:"foreignkey:role_id;association_foreignkey:id;" pex:"user:r"`

	CreatedAt *time.Time `json:"created_at,omitempty" sql:"type:datetime(6)"`
	UpdatedAt *time.Time `json:"updated_at,omitempty" sql:"type:datetime(6)"`
	DeletedAt *time.Time `json:"deleted_at,omitempty" sql:"type:datetime(6)" pex:"user:"`
}

// PasswordSession -
type PasswordSession struct {
	ID				int				`json:"id" gorm:"column:id; type:int unsigned auto_increment; primary_key; unique;"`
	Secret			string			`json:"secret" sql:"not null" gorm:"column:secret"`
	UserID			int				`json:"user_id" sql:"not null" gorm:"column:user_id"`
	CreatedAt       time.Time       `json:"created_at" sql:"type:datetime(6)"`
	UpdatedAt       time.Time       `json:"updated_at" sql:"type:datetime(6)"`
	DeletedAt       *time.Time      `json:"deleted_at,omitempty" sql:"type:datetime(6)"`
}

func (User) TableName() string {
	return "user"
}

func (PasswordSession) TableName() string {
	return "password_session"
}

func (u User) GetOutput(usertype string) (ru interface{}) {
	ru = gopex.ExtractFields(u, usertype, gopex.ActionRead)
	return
}

// Create -
func (u *User) Create() error {
	app := base.GetApplication()
	// SpanStruct := session.GetSpan()
	// ChildSpan := SpanStruct.CreateChildSpan("Create")
	// defer ChildSpan.AddSpan()

	if err := u.Validation(); err != nil {
		return err
	}

	db := app.DB

	if result := db.Debug().Create(&u); result.Error != nil {
		app.Logger.Error(result.Error)

		return result.Error
	}

	return nil
}

func (passwordSession *PasswordSession) Create() (id int, err error) {
	app := core.GetApplication()
	db := app.DB
	validate := validator.New()
	err = validate.Struct(passwordSession)
	if err != nil {
		app.Logger.Error(err.Error())
		err = errors.New("Invalid json")
		return
	}

	if result := db.Create(&passwordSession); result.Error != nil {
		err = result.Error
		return
	}
	id = passwordSession.ID
	return
}

// Validation -
func (u *User) Validation() error {
	return nil
}

// Update
func (user *User) Edit() (err error) {
	app := core.GetApplication()
	db := app.DB
	validate := validator.New()
	err = validate.Struct(user)
	if err != nil {
		app.Logger.Error(err.Error())
		err = errors.New("invalid json")
		return
	}
	if result := db.Model(User{ID: user.ID}).Save(&user); result.Error != nil {
		err = result.Error
		return
	}
	return
}

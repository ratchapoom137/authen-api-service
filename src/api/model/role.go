package model

import (
	"time"

	gopex "github.com/joaosilva2095/go-pex"
)

// Role -
type Role struct {
	ID         int          `json:"id,omitempty" gorm:"column:id;type:int unsigned auto_increment;primary_key;unique;"`
	Name       string       `json:"name" gorm:"column:name;not null;unique;"`
	Permission []Permission `json:"permissions,omitempty" gorm:"many2many:role_permission;association_foreignkey:id;foreignkey:id;save_associations:false;PRELOAD:true" pex:"user:r"`
	CreatedAt  *time.Time   `json:"created_at,omitempty" sql:"type:datetime(6)" pex:"user:"`
	UpdatedAt  *time.Time   `json:"updated_at,omitempty" sql:"type:datetime(6)" pex:"user:"`
	DeletedAt  *time.Time   `json:"deleted_at,omitempty" sql:"type:datetime(6)" pex:"user:"`
}

// Permission -
type Permission struct {
	ID       int    `json:"id,omitempty" gorm:"column:id;type:int unsigned auto_increment;primary_key;unique;" pex:"user:"`
	Module   string `json:"module" gorm:"column:module;not null;" pex:"user:r"`
	Function string `json:"function" gorm:"column:function;not null;" pex:"user:r"`
	// Role      Role       `json:"role" gorm:"many2many:role_permission;foreignkey:id;association_foreignkey:id;save_associations:false"`
	CreatedAt *time.Time `json:"created_at,omitempty" sql:"type:datetime(6)" pex:"user:"`
	UpdatedAt *time.Time `json:"updated_at,omitempty" sql:"type:datetime(6)" pex:"user:"`
	DeletedAt *time.Time `json:"deleted_at,omitempty" sql:"type:datetime(6)" pex:"user:"`
}

// RolePermission -
type RolePermission struct {
	RoleID       int `json:"role_id" gorm:"column:role_id;type:int unsigned;primary_key;"`
	PermissionID int `json:"permission_id" gorm:"column:permission_id;type:int unsigned;primary_key;"`
}

func (Role) TableName() string {
	return "role"
}

func (Permission) TableName() string {
	return "permission"
}

func (RolePermission) TableName() string {
	return "role_permission"
}

func (r Role) GetOutput(usertype string) (ro interface{}) {
	ro = gopex.ExtractFields(r, usertype, gopex.ActionRead)

	return
}

func (p Permission) GetOutput(format string) (rp Permission) {
	if format == "full" {
		rp.ID = p.ID
		rp.CreatedAt = p.CreatedAt
		rp.UpdatedAt = p.UpdatedAt
	}

	rp.Module = p.Module
	rp.Function = p.Function
	return
}

package base

import (
	"dv.co.th/hrms/core"
)

var theApp *core.Application

func init() {

	if theApp == nil {
		theApp = core.NewApplication()
	}
}

func GetApplication() *core.Application {
	return theApp
}

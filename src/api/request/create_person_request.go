package request

import (
	"time"

	"github.com/shopspring/decimal"
)

type APICreatePerson struct {
	PersonNumber string          `json:"PersonNumber"`
	IDCardNumber string          `json:"IdCardNumber"`
	IDCardType   string          `json:"IdCardType"`
	StartDate    time.Time       `json:"StartDate"`
	EndDate      time.Time       `json:"EndDate"`
	Salary       decimal.Decimal `json:"amount"`
}

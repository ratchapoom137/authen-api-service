package restinterface

// Using for Swagger definition generation
type APIResult struct {
	Data   interface{}     `json:"data,omitempty"`
	Total  int             `json:"total,omitempty"`
	Status APIResultStatus `json:"status,omitempty"`
}

type APIResultStatus struct {
	Code    string `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
}

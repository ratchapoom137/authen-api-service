package restinterface

import (
	"api/base"
	"api/service"

	"dv.co.th/hrms/core/rest"
	"github.com/kataras/iris"
)

// Login godoc
// @Summary Get the latest person properties of a specified personn
// @Description
// @Accept multipart/form-data
// @Tags public:user
// @Accept json
// @Produce json
// @Param username formData string true "Username[admin|hr]"
// @Param password formData string true "Password"
// @Success 200 {object} restinterface.APIResult ""
// @Failure 401 {object} restinterface.APIResult ""
// @Failure 500 {object} restinterface.APIResult ""
// @Router /login [post]
func Login(ctx iris.Context) {
	app := base.GetApplication()

	username := ctx.FormValueDefault("username", "dummy")
	password := ctx.FormValueDefault("password", "dummy")

	app.Logger.Infof("Login with %s", username)
	User, ok, result := service.GetUserFromUserNameAndPassword(username, password)

	if ok {
		resultJSON := rest.APIResult{
			Data: User,
			Status: rest.APIResultStatus{
				Code:    "SUCCESS",
				Message: "Login success",
			},
		}
		rest.Success(ctx, resultJSON)
		return
	}

	rest.Error(ctx, rest.APIResult{
		Status: rest.APIResultStatus{
			Code:    result.Code,
			Message: result.Message,
		},
	})
	return
}

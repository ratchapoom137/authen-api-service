package restinterface

import (
	"api/constant"
	"api/model"
	"api/service"

	"dv.co.th/hrms/core/rest"
	"github.com/kataras/iris"
)

// GetRole godoc
// @Summary Get the latest person properties of a specified personn
// @Description
// @Accept multipart/form-data
// @Tags public:user
// @Accept json
// @Produce json
// @Success 200 {object} restinterface.APIResult ""
// @Failure 401 {object} restinterface.APIResult ""
// @Failure 500 {object} restinterface.APIResult ""
// @Router /role [get]
func GetRoles(ctx iris.Context) {
	name := ctx.FormValue("name")
	filter := map[string]interface{}{}

	if name != "" {
		filter["name"] = name
	}
	
	Role, ok, result := service.GetRole(filter)

	if ok {
		resultJSON := rest.APIResult{
			Data:  Role,
			Total: len(Role.([]model.Role)),
			Status: rest.APIResultStatus{
				Code:    constant.ReturnConst["Success"].Code,
				Message: "User creation success",
			},
		}
		rest.SuccessSearch(ctx, resultJSON)
		return
	}

	rest.Error(ctx, rest.APIResult{
		Status: rest.APIResultStatus{
			Message: result.Message,
			Code:    result.Code,
		},
	})
	return
}
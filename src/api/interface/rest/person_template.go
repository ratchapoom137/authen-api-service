package restinterface

import (
	"api/base"
	"strconv"

	"dv.co.th/hrms/core/rest"
	"github.com/kataras/iris"
)

// PostPerson godoc
/*
// @Summary Add a new person
// @Description
// @Tags admin:person
// @Accept json
// @Produce json
// @Param user body request.APICreatePerson true "Add person"
// @Success 200 {object} rest.APIResult ""
// @Failure 401 {object} rest.APIResult ""
// @Failure 500 {object} rest.APIResult ""
// @Router /admin/person [post]
*/
func PostPerson(ctx iris.Context) {

	//app := config.GetApplication()
}

// GetPerson godoc
/*
// @Summary Get the latest person properties of a specified personn
// @Description
// @Tags admin:person
// @Accept json
// @Produce json
// @Param user body request.APICreatePerson true "Get person"
// @Success 200 {object} rest.APIResult ""
// @Failure 401 {object} rest.APIResult ""
// @Failure 500 {object} rest.APIResult ""
// @Router /admin/person [get]
*/
func GetPerson(ctx iris.Context) {
	app := base.GetApplication()

	personID, _ := strconv.Atoi(ctx.Params().Get("Id"))
	app.Logger.Debug("GetPerson Id = ", personID)

	URL := "http://vmvkk.mocklab.io/person/6"
	result, _ := rest.CallRestAPIWithOption("GET", URL, nil)

	resultJSON := rest.APIResult{
		Data: result.Data,
		Status: rest.APIResultStatus{
			Message: "Hello From John",
		},
	}

	rest.Success(ctx, resultJSON)
}

// PutPerson godoc
/*
// @Summary Update the person properties of a specified personn
// @Description
// @Tags admin:person
// @Accept json
// @Produce json
// @Param user body request.APICreatePerson true "Put pserson"
// @Success 200 {object} rest.APIResult ""
// @Failure 401 {object} rest.APIResult ""
// @Failure 500 {object} rest.APIResult ""
// @Router /admin/person [put]
*/
func PutPerson(ctx iris.Context) {
	app := base.GetApplication()

	personId, _ := strconv.Atoi(ctx.Params().Get("Id"))
	app.Logger.Debug("PutPerson Id = ", personId)
}

package restinterface

import (
	"api/base"
	"api/constant"
	"api/service"
	"api/model"
	"strconv"

	"dv.co.th/hrms/core/rest"
	"github.com/kataras/iris"
)

// UserCreateInput input struct for create user
type UserCreateInput struct {
	Username string `json:"username"`
	Password string `json:"password"`
	RoleID   int    `json:"role_id"`
}

// UserCreate godoc
// @Summary Get the latest person properties of a specified personn
// @Description
// @Accept multipart/form-data
// @Tags public:user
// @Accept json
// @Produce json
// @Param user body restinterface.UserCreateInput true "UserCreateInput"
// @Success 200 {object} restinterface.APIResult ""
// @Failure 401 {object} restinterface.APIResult ""
// @Failure 500 {object} restinterface.APIResult ""
// @Router /user [post]
func UserCreate(ctx iris.Context) {
	app := base.GetApplication()

	var userInput UserCreateInput
	if err := ctx.ReadJSON(&userInput); err != nil {
		app.Logger.Error(err.Error())

		rest.Error(ctx, rest.APIResult{
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["InvalidParamConst"].Message,
				Code:    constant.ReturnConst["InvalidParamConst"].Code,
			},
		})
		return
	}

	username := userInput.Username
	password := userInput.Password
	var roleID int
	if roleID = userInput.RoleID; roleID <= 0 {
		roleID = 3
	}

	User, ok, result := service.CreateUser(username, password, roleID)

	if ok {
		resultJSON := rest.APIResult{
			Data: User,
			Status: rest.APIResultStatus{
				Code:    constant.ReturnConst["Success"].Code,
				Message: "User creation success",
			},
		}
		rest.Success(ctx, resultJSON)
		return
	}

	rest.Error(ctx, rest.APIResult{
		Status: rest.APIResultStatus{
			Message: result.Message,
			Code:    result.Code,
		},
	})
	return
}

// GetUserAccountByID godoc
// @Summary Get user account from given id
// @Description
// @Accept multipart/form-data
// @Tags public:user
// @Accept json
// @Produce json
// @Param userid path int true "User ID"
// @Success 200 {object} restinterface.APIResult ""
// @Failure 401 {object} restinterface.APIResult ""
// @Failure 500 {object} restinterface.APIResult ""
// @Router /user/{id} [get]
func GetUserByID(ctx iris.Context) {
	app := base.GetApplication()

	UserID, _ := strconv.Atoi(ctx.Params().Get("UserID"))

	app.Logger.Infof("Get user with %s", UserID)
	User, ok, result := service.GetUserFromUserID(UserID)

	if ok {
		resultJSON := rest.APIResult{
			Data: User,
			Status: rest.APIResultStatus{
				Code:    "SUCCESS",
				Message: "Login success",
			},
		}
		rest.Success(ctx, resultJSON)
		return
	}

	rest.Error(ctx, rest.APIResult{
		Status: rest.APIResultStatus{
			Code:    result.Code,
			Message: result.Message,
		},
	})
	return
}

// GetUsers godoc
// @Summary Get users
// @Description
// @Router /auth/users [get]
func GetUsers(ctx iris.Context) {
	filter := map[string]interface{}{
		"username":        ctx.URLParam("username"),
		"search_username": ctx.URLParam("search_username"),
		"limit":           ctx.URLParamDefault("limit", "9999"),
		"offset":          ctx.URLParamDefault("offset", "0"),
		"role_name":       ctx.URLParam("role_name"),
	}
	outputResult, ok, total := service.GetUserList(filter)

	if !ok {
		errResponse := rest.APIResult{
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["SearchUserListFailedConst"].Message,
				Code:    constant.ReturnConst["SearchUserListFailedConst"].Code,
			},
		}
		rest.Fail(ctx, errResponse, 400)
		return
	}

	successResponse := rest.APIResult{
		Data:  outputResult,
		Total: total,
		Status: rest.APIResultStatus{
			Message: constant.ReturnConst["SearchUserListSuccessConst"].Message,
			Code:    constant.ReturnConst["SearchUserListSuccessConst"].Code,
		},
	}
	rest.SuccessSearch(ctx, successResponse)
	return
}

// DeleteUser godoc
// @Summary Delete user
// @Description
// @Router /auth/user [delete]
func DeleteUser(ctx iris.Context) {
	userID, _ := strconv.Atoi(ctx.Params().Get("id"))

	err := service.DeleteUser(userID)

	if err != nil {
		err := rest.APIResult{
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["DeleteUserFailedConst"].Message,
				Code:    constant.ReturnConst["DeleteUserFailedConst"].Code,
			},
		}
		rest.Fail(ctx, err, 404)
		return
	}

	successResponse := rest.APIResult{
		Data: userID,
		Status: rest.APIResultStatus{
			Message: constant.ReturnConst["DeleteUserSuccessConst"].Message,
			Code:    constant.ReturnConst["DeleteUserSuccessConst"].Code,
		},
	}
	rest.SuccessSearch(ctx, successResponse)
}

// Put godoc
// @Summary Update user 
// @Description
// @Router /auth/user [put]
func PutUser(ctx iris.Context) {
	var userInfo model.User
	err := ctx.ReadJSON(&userInfo)

	if err != nil {
		errResponse := rest.APIResult{
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["ReadUserDetailJsonFailedConst"].Message,
				Code:    constant.ReturnConst["ReadUserDetailJsonFailedConst"].Code,
			},
		}
		rest.Fail(ctx, errResponse, 400)
		return
	}

	userID, _ := strconv.Atoi(ctx.Params().Get("id"))
	outputError := service.EditUserInfo(userID, userInfo)

	if outputError != nil {
		err := rest.APIResult{
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["UpdateUserFailedConst"].Message,
				Code:    constant.ReturnConst["UpdateUserFailedConst"].Code,
			},
		}
		rest.Fail(ctx, err, 404)
		return
	}

	successResponse := rest.APIResult{
		Data: userID,
		Status: rest.APIResultStatus{
			Message: constant.ReturnConst["UpdateUserSuccessConst"].Message,
			Code:    constant.ReturnConst["UpdateUserSuccessConst"].Code,
		},
	}
	rest.SuccessSearch(ctx, successResponse)
}

// PostPasswordSession godoc
// @Summary Add a password session for user account
// @Description
// @Router /auth/password-session [post]
func PostPasswordSession(ctx iris.Context) {
	var password model.PasswordSession
	err := ctx.ReadJSON(&password)

	if err != nil {
		errResponse := rest.APIResult{
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["PasswordSessionNotFoundConst"].Message,
				Code:    constant.ReturnConst["PasswordSessionNotFoundConst"].Code,
			},
		}
		rest.Fail(ctx, errResponse, 400)
		return
	}

	outputResult, err := service.CreatePasswordSession(password)

	if err != nil {
		errResponse := rest.APIResult{
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["CreatePasswordSessionFailedConst"].Message,
				Code:    constant.ReturnConst["CreatePasswordSessionFailedConst"].Code,
			},
		}
		rest.Fail(ctx, errResponse, 400)
		return
	}

	successResponse := rest.APIResult{
		Data: outputResult,
		Status: rest.APIResultStatus{
			Message: constant.ReturnConst["CreatePasswordSessionSuccessConst"].Message,
			Code:    constant.ReturnConst["CreatePasswordSessionSuccessConst"].Code,
		},
	}
	rest.Success(ctx, successResponse)
}

// GetPasswordSession godoc
// @Summary Get password session by id
// @Description
// @Router /auth/password-session [post]
func GetPasswordSession(ctx iris.Context) {
	passwordSessionID, _ := strconv.Atoi(ctx.Params().Get("id"))

	passwordSessionOutput, err := service.GetPasswordSessionByID(passwordSessionID)

	if err != nil {
		errResponse := rest.APIResult{
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["PasswordSessionNotFoundConst"].Message,
				Code:    constant.ReturnConst["PasswordSessionNotFoundConst"].Code,
			},
		}
		rest.Fail(ctx, errResponse, 400)
		return
	}

	successResponse := rest.APIResult{
		Data: passwordSessionOutput,
		Status: rest.APIResultStatus{
			Message: constant.ReturnConst["SearchPasswordSessionSuccessConst"].Message,
			Code:    constant.ReturnConst["SearchPasswordSessionSuccessConst"].Code,
		},
	}
	rest.Success(ctx, successResponse)
}

// GetPasswordSessions godoc
// @Summary Get password sessions
// @Description
// @Router /auth/password-sessions [get]
func GetPasswordSessions(ctx iris.Context) {
	filter := map[string]interface{}{
		"secret": ctx.URLParam("secret"),
		"user_id": ctx.URLParam("user_id"),
		"limit": ctx.URLParamDefault("limit", "99999"),
		"offset": ctx.URLParamDefault("offset", "0"),
	}

	passwordSessions, total, err := service.GetPasswordSessionList(filter)
	if err != nil {
		errResponse := rest.APIResult{
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["PasswordSessionNotFoundConst"].Message,
				Code:    constant.ReturnConst["PasswordSessionNotFoundConst"].Code,
			},
		}
		rest.Fail(ctx, errResponse, 404)
		return
	}

	successResponse := rest.APIResult{
		Data:  passwordSessions,
		Total: total,
		Status: rest.APIResultStatus{
			Message: constant.ReturnConst["SearchPasswordSessionSuccessConst"].Message,
			Code:    constant.ReturnConst["SearchPasswordSessionSuccessConst"].Code,
		},
	}
	rest.SuccessSearch(ctx, successResponse)
}

// Delete godoc
// @Summary Delete password session
// @Description
// @Router /auth/password-session [delete]
func DeletePasswordSession(ctx iris.Context) {
	passwordSessionID, _ := strconv.Atoi(ctx.Params().Get("id"))

	err := service.DeletePasswordSession(passwordSessionID)

	if err != nil {
		err := rest.APIResult{
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["DeletePasswordSessionFailedConst"].Message,
				Code:    constant.ReturnConst["DeletePasswordSessionFailedConst"].Code,
			},
		}
		rest.Fail(ctx, err, 404)
		return
	}
	
	successResponse := rest.APIResult{
		Data: passwordSessionID,
		Status: rest.APIResultStatus{
			Message: constant.ReturnConst["DeletePasswordSessionSuccessConst"].Message,
			Code:    constant.ReturnConst["DeletePasswordSessionSuccessConst"].Code,
		},
	}
	rest.SuccessSearch(ctx, successResponse)
}

package migration

import (
	"api/base"
	"api/model"

	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

func Execute() {
	app := base.GetApplication()
	db := app.DB

	db.LogMode(true)

	m := gormigrate.New(db, gormigrate.DefaultOptions, []*gormigrate.Migration{
		{
			ID:       "201906232036",
			Migrate:  Migrate_201906232036,
			Rollback: Rollback_201906232036,
		},
		{
			ID:       "201908142036",
			Migrate:  Seed_201908142036,
			Rollback: Rollback_201908142036,
		},
	})

	if err := m.Migrate(); err != nil {
		app.Logger.Error("Could not migrate: ", err)
		return
	}

	app.Logger.Info("Migration done.")
}

func Migrate_201906232036(tx *gorm.DB) error {
	tx = tx.Begin()

	tx.AutoMigrate(
		&model.User{},
		&model.RolePermission{},
		&model.Role{},
		&model.Permission{},
		&model.PasswordSession{},
	)

	return tx.Commit().Error
}

func Rollback_201906232036(tx *gorm.DB) error {
	return tx.Rollback().Error
}

func Seed_201908142036(tx *gorm.DB) error {
	tx = tx.Begin()

	// permission module create edit delete read
	dashboardReadPermission := model.Permission{
		Module:   "dashboard",
		Function: "read",
	}
	userReadPermission := model.Permission{
		Module:   "user",
		Function: "read",
	}
	// OM Company Permission 
	companyReadPermission := model.Permission{
		Module:   "company",
		Function: "read",
	}
	companyCreatePermission := model.Permission{
		Module:   "company",
		Function: "create",
	}
	companyAddressCreatePermission := model.Permission{
		Module:   "company_address",
		Function: "create",
	}
	companyEditPermission := model.Permission{
		Module:   "company",
		Function: "edit",
	}
	companyAddressDeletePermission := model.Permission{
		Module:   "company_address",
		Function: "delete",
	}
	myCompanyReadPermission := model.Permission{
		Module:   "my_company",
		Function: "read",
	}
	// OM Org-Level Permission 
	orgLevelReadPermission := model.Permission{
		Module:   "org_level",
		Function: "read",
	}
	orgLevelCreatePermission := model.Permission{
		Module:   "org_level",
		Function: "create",
	}
	orgLevelEditPermission := model.Permission{
		Module:   "org_level",
		Function: "edit",
	}
	orgLevelDeletePermission := model.Permission{
		Module:   "org_level",
		Function: "delete",
	}
	// OM Org-Unit Permission 
	orgUnitReadPermission := model.Permission{
		Module:   "org_unit",
		Function: "read",
	}
	orgUnitCreatePermission := model.Permission{
		Module:   "org_unit",
		Function: "create",
	}
	orgUnitEditPermission := model.Permission{
		Module:   "org_unit",
		Function: "edit",
	}
	orgUnitDeletePermission := model.Permission{
		Module:   "org_unit",
		Function: "delete",
	}
	// OM Position Permission 
	employeePositionReadPermission := model.Permission{
		Module:   "employee_position",
		Function: "read",
	}
	employeePositionCreatePermission := model.Permission{
		Module:   "employee_position",
		Function: "create",
	}
	employeePositionEditPermission := model.Permission{
		Module:   "employee_position",
		Function: "edit",
	}
	employeePositionDeletePermission := model.Permission{
		Module:   "employee_position",
		Function: "delete",
	}
	// OM Approval Configuretion Permission 
	approvalConfigReadPermission := model.Permission{
		Module:   "approval_config",
		Function: "read",
	}
	approvalConfigCreatePermission := model.Permission{
		Module:   "approval_config",
		Function: "create",
	}
	approvalConfigEditPermission := model.Permission{
		Module:   "approval_config",
		Function: "edit",
	}
	approvalConfigDeletePermission := model.Permission{
		Module:   "approval_config",
		Function: "delete",
	}

	//PA Employee Permission
	employeeCreatePermission := model.Permission{
		Module:   "employee",
		Function: "create",
	}
	employeeReadPermission := model.Permission{
		Module:   "employee",
		Function: "read",
	}
	employeeEditPermission := model.Permission{
		Module:   "employee",
		Function: "edit",
	}
	profileReadPermission := model.Permission{
		Module:   "profile",
		Function: "read",
	}
	employeeSearchPermission := model.Permission{
		Module:   "employee",
		Function: "search",
	}

	//AA Asset Category Permission 
	assetCategoryReadPermission := model.Permission{
		Module:   "asset_category",
		Function: "read",
	}
	assetCategoryCreatePermission := model.Permission{
		Module:   "asset_category",
		Function: "create",
	}
	assetCategoryEditPermission := model.Permission{
		Module:   "asset_category",
		Function: "edit",
	}
	assetCategoryDeletePermission := model.Permission{
		Module:   "asset_category",
		Function: "delete",
	}
	//AA Asset Permission 
	assetReadPermission := model.Permission{
		Module:   "asset",
		Function: "read",
	}
	assetCreatePermission := model.Permission{
		Module:   "asset",
		Function: "create",
	}
	assetEditPermission := model.Permission{
		Module:   "asset",
		Function: "edit",
	}
	assetDeletePermission := model.Permission{
		Module:   "asset",
		Function: "delete",
	}
	//AA Asset Operation Permission 
	assetOperationReadPermission := model.Permission{
		Module:   "asset_operation",
		Function: "read",
	}
	assetOperationCreatePermission := model.Permission{
		Module:   "asset_operation",
		Function: "create",
	}
	assetOperationEditPermission := model.Permission{
		Module:   "asset_operation",
		Function: "edit",
	}

	// TMA Holiday Preset Permission
	holidayPresetReadPermission := model.Permission{
		Module:   "holiday_preset",
		Function: "read",
	}
	holidayPresetCreatePermission := model.Permission{
		Module:   "holiday_preset",
		Function: "create",
	}
	holidayPresetEditPermission := model.Permission{
		Module:   "holiday_preset",
		Function: "edit",
	}
	holidayPresetDeletePermission := model.Permission{
		Module:   "holiday_preset",
		Function: "delete",
	}
	// TMA Company Holiday Permission
	holidayCompanyReadPermission := model.Permission{
		Module:   "company_holiday",
		Function: "read",
	}
	holidayCompanyCreatePermission := model.Permission{
		Module:   "company_holiday",
		Function: "create",
	}
	holidayCompanyEditPermission := model.Permission{
		Module:   "company_holiday",
		Function: "edit",
	}
	holidayCompanyDeletePermission := model.Permission{
		Module:   "company_holiday",
		Function: "delete",
	}
	// TMA Work Preset Permission
	workPresetReadPermission := model.Permission{
		Module:   "work_preset",
		Function: "read",
	}
	workPresetCreatePermission := model.Permission{
		Module:   "work_preset",
		Function: "create",
	}
	workPresetEditPermission := model.Permission{
		Module:   "work_preset",
		Function: "edit",
	}
	workPresetDeletePermission := model.Permission{
		Module:   "work_preset",
		Function: "delete",
	}
	// TMA Charge Code Permission
	chargeCodeReadPermission := model.Permission{
		Module:   "charge_code",
		Function: "read",
	}
	chargeCodeCreatePermission := model.Permission{
		Module:   "charge_code",
		Function: "create",
	}
	chargeCodeEditPermission := model.Permission{
		Module:   "charge_code",
		Function: "edit",
	}
	chargeCodeDeletePermission := model.Permission{
		Module:   "charge_code",
		Function: "delete",
	}
	// TMA Work Report Permission
	workReportReadPermission := model.Permission{
		Module:   "work_report",
		Function: "read",
	}
	workReportCreatePermission := model.Permission{
		Module:   "work_report",
		Function: "create",
	}
	workReportEditPermission := model.Permission{
		Module:   "work_report",
		Function: "edit",
	}
	workReportDeletePermission := model.Permission{
		Module:   "work_report",
		Function: "delete",
	}
	// TMA Time Sheet Permission
	timeSheetReadPermission := model.Permission{
		Module:   "time_sheet",
		Function: "read",
	}
	timeSheetCreatePermission := model.Permission{
		Module:   "time_sheet",
		Function: "create",
	}
	timeSheetEditPermission := model.Permission{
		Module:   "time_sheet",
		Function: "edit",
	}
	// TMA Work Adjustment Permission
	workAdjustmentReadPermission := model.Permission{
		Module:   "work_adjustment",
		Function: "read",
	}
	workAdjustmentCreatePermission := model.Permission{
		Module:   "work_adjustment",
		Function: "create",
	}
	workAdjustmentEditPermission := model.Permission{
		Module:   "work_adjustment",
		Function: "edit",
	}
	// TMA Period Permission Permission
	ReadPeriodPermission := model.Permission{
		Module:   "period",
		Function: "read",
	}
	//TMA My Work Submission Permission
	myWorkSubmissionReadPermission := model.Permission{
		Module:   "my_work_submission",
		Function: "read",
	}
	//TMA Work Submission Permission
	workSubmissionReadPermission := model.Permission{
		Module:   "work_submission",
		Function: "read",
	}
	//TMA Member Permission
	memberReadPermission := model.Permission{
		Module:   "member",
		Function: "read",
	}

	//TML Leave Type Permission
	leaveTypeReadPermission := model.Permission{
		Module:   "leave_type",
		Function: "read",
	}
	leaveTypeCreatePermission := model.Permission{
		Module:   "leave_type",
		Function: "create",
	}
	leaveTypeEditPermission := model.Permission{
		Module:   "leave_type",
		Function: "edit",
	}
	//TML Leave Request Permission
	leaveRequestReadPermission := model.Permission{
		Module:   "leave_request",
		Function: "read",
	}
	leaveRequestCreatePermission := model.Permission{
		Module:   "leave_request",
		Function: "create",
	}
	leaveRequestEditPermission := model.Permission{
		Module:   "leave_request",
		Function: "edit",
	}
	//TML Leave Report Permission
	leaveReportReadPermission := model.Permission{
		Module:   "leave_report",
		Function: "read",
	}
	//TML Employee Quota Permission
	employeeQuotaReadPermission := model.Permission{
		Module:   "employee_quota",
		Function: "read",
	}
	employeeQuotaEditPermission := model.Permission{
		Module:   "employee_quota",
		Function: "edit",
	}
	employeeQuotaCreatePermission := model.Permission{
		Module:   "employee_quota",
		Function: "create",
	}
	//TML Leave Approval Permission
	leaveApprovalReadPermission := model.Permission{
		Module:   "leave_approval",
		Function: "read",
	}
	leaveApprovalEditPermission := model.Permission{
		Module:   "leave_approval",
		Function: "edit",
	}

	// Company Request Management Permission
	registrationRequestReadPermission := model.Permission{
		Module:   "registration_request",
		Function: "read",
	}
	registrationRequestEditPermission := model.Permission{
		Module:   "registration_request",
		Function: "edit",
	}
	terminationRequestCreatePermission := model.Permission{
		Module:   "termination_request",
		Function: "create",
	}
	terminationRequestReadPermission := model.Permission{
		Module:   "termination_request",
		Function: "read",
	}
	terminationRequestEditPermission := model.Permission{
		Module:   "termination_request",
		Function: "edit",
	}
	myTerminationRequestEditPermission := model.Permission{
		Module:   "my_termination_request",
		Function: "read",
	}
	
	// Account Management Permission
	accountReadPermission := model.Permission{
		Module:   "account",
		Function: "read",
	}
	accountCreatePermission := model.Permission{
		Module:   "account",
		Function: "create",
	}
	accountEditPermission := model.Permission{
		Module:   "account",
		Function: "edit",
	}
	accountDeletePermission := model.Permission{
		Module:   "account",
		Function: "delete",
	}

	// My Account Profile Management Permission
	myAccountProfileReadPermission := model.Permission{
		Module:   "my_account",
		Function: "read",
	}
	PasswordEditPermission := model.Permission{
		Module:   "password",
		Function: "edit",
	}

	// Reimbursement Type Management Permission
	reimbursementTypeReadPermission := model.Permission{
		Module:   "reimbursement_type",
		Function: "read",
	}
	reimbursementTypeEditPermission := model.Permission{
		Module:   "reimbursement_type",
		Function: "edit",
	}

	// Reimbursement Request Management Permission
	reimbursementRequestReadPermission := model.Permission{
		Module:   "reimbursement_request",
		Function: "read",
	}
	reimbursementRequestCreatePermission := model.Permission{
		Module:   "reimbursement_request",
		Function: "create",
	}
	reimbursementRequestEditPermission := model.Permission{
		Module:   "reimbursement_request",
		Function: "edit",
	}

	// My Reimbursement Request Management Permission
	myReimbursementRequestReadPermission := model.Permission{
		Module:   "my_reimbursement_request",
		Function: "read",
	}
	myReimbursementRequestEditPermission := model.Permission{
		Module:   "my_reimbursement_request",
		Function: "edit",
	}

	// Reviewed Reimbursement Request Management Permission
	reviewedReimbursementRequestReadPermission := model.Permission{
		Module:   "reviewed_reimbursement_request",
		Function: "read",
	}
	reviewedReimbursementRequestEditPermission := model.Permission{
		Module:   "reviewed_reimbursement_request",
		Function: "edit",
	}

	// Approved Reimbursement Request Management Permission
	approvedReimbursementRequestReadPermission := model.Permission{
		Module:   "approved_reimbursement_request",
		Function: "read",
	}
	approvedReimbursementRequestEditPermission := model.Permission{
		Module:   "approved_reimbursement_request",
		Function: "edit",
	}

	// Reimbursement Report Permission
	reimbursementReportEditPermission := model.Permission{
		Module:   "reimbursement_report",
		Function: "read",
	}

	// My Riembursement Perdiem Rate
	myPerdiemRate := model.Permission{
		Module:   "my_perdiem_rate",
		Function: "read",
	}

	//Roles
	adminRole := model.Role{
		Name: "admin",
	}
	hrRole := model.Role{
		Name: "hr",
	}
	employeeRole := model.Role{
		Name: "employee",
	}
	backOfficeAdminRole := model.Role{
		Name: "back_office_admin",
	}
	managerRole := model.Role{
		Name: "manager",
	}

	// Password Session
	passwordSeesion1 := model.PasswordSession {
		Secret: "bBGckjsOpC7fNow3WEqueh3NutB8RNEV",
		UserID:	2,
	}
	passwordSeesion2 := model.PasswordSession {
		Secret: "FidPp06F2R1WAl7kbMVXz4zd7Oqn57P5",
		UserID:	3,
	}
	passwordSeesion3 := model.PasswordSession {
		Secret: "9wQNm3EtBqG6J7u8nfFsLzsgeoSfxwAy",
		UserID:	4,
	}
	passwordSeesion4 := model.PasswordSession {
		Secret: "8SpX3LjLVJqvnadgUb6C0ir4zh64ukZP",
		UserID:	5,
	}
	passwordSeesion5 := model.PasswordSession {
		Secret: "TVVPCYjMVVQXK2r8k4nwEt6HtO99oo7A",
		UserID:	6,
	}
	passwordSeesion6 := model.PasswordSession {
		Secret: "6D7XgYlkr2bXKKA5cfqrZV7DZLnKzFcJ",
		UserID:	7,
	}
	passwordSeesion7 := model.PasswordSession {
		Secret: "l2hVrTTJeuiqZ5LI7cZE0Y5tcfgJgRcB",
		UserID:	8,
	}
	passwordSeesion8 := model.PasswordSession {
		Secret: "NcGQjAixLuTEgkYjEphFrayAzbilAHPN",
		UserID:	9,
	}
	passwordSeesion9 := model.PasswordSession {
		Secret: "k5v7HuVLOPhFmhgM4UkURvCIT7sMS0d8",
		UserID:	10,
	}
	passwordSeesion10 := model.PasswordSession {
		Secret: "fGtweXx1b6nUQBxsFKMtF3hfJ7IXmsvp",
		UserID:	11,
	}
	passwordSeesion11 := model.PasswordSession {
		Secret: "B5q5g4mKaVo8x83j28XFcbaU66il0kDA",
		UserID:	12,
	}
	passwordSeesion12 := model.PasswordSession {
		Secret: "1BPjJsH8CtXEmeIHGWT80K8LyKdv3cxF",
		UserID:	13,
	}
	passwordSeesion13 := model.PasswordSession {
		Secret: "bUOo4zfmmJM029yqu2HVCdvSQF3dEyA0",
		UserID:	14,
	}
	passwordSeesion14 := model.PasswordSession {
		Secret: "6255E9QxYy9lntHviuSM8ethgmL9keD8",
		UserID:	15,
	}
	passwordSeesion15 := model.PasswordSession {
		Secret: "An4V7lRFGIWw0q7UR8pkJO4DGZiWcRRp",
		UserID:	16,
	}
	passwordSeesion16 := model.PasswordSession {
		Secret: "5PCHDKycNKif3kyVEdMEmQ2k7XBUOWsb",
		UserID:	17,
	}
	passwordSeesion17 := model.PasswordSession {
		Secret: "k6AAx0qZRHHhlLBrrWvqmVGYup8QmaEn",
		UserID:	18,
	}
	passwordSeesion18 := model.PasswordSession {
		Secret: "5qjCglX7dYrYD9NGqFSXKRG93Za0M7mk",
		UserID:	19,
	}
	passwordSeesion19 := model.PasswordSession {
		Secret: "jr5jrcnQYHMdqtqeHJTdsJrcWkYDhENd",
		UserID:	20,
	}
	passwordSeesion20 := model.PasswordSession {
		Secret: "jr5jrcnQYHUUiqtqeHJTdsJrcWkYDhENd",
		UserID:	21,
	}	
	passwordSeesion21 := model.PasswordSession {
		Secret: "br6jrcnQYHUUiqtqeHJTdsJrcWkYDhENd",
		UserID:	22,
	}
	//create permission
	tx.Create(&dashboardReadPermission)
	tx.Create(&userReadPermission)
	// Create OM Company Permission
	tx.Create(&companyReadPermission)
	tx.Create(&companyCreatePermission)
	tx.Create(&companyAddressCreatePermission)
	tx.Create(&companyEditPermission)
	tx.Create(&companyAddressDeletePermission)
	tx.Create(&myCompanyReadPermission)

	// Create OM Org Level Permission
	tx.Create(&orgLevelReadPermission)
	tx.Create(&orgLevelCreatePermission)
	tx.Create(&orgLevelEditPermission)
	tx.Create(&orgLevelDeletePermission)
	// Create OM Org Unit Permission
	tx.Create(&orgUnitReadPermission)
	tx.Create(&orgUnitCreatePermission)
	tx.Create(&orgUnitEditPermission)
	tx.Create(&orgUnitDeletePermission)
	// Create OM Employee Position Permission
	tx.Create(&employeePositionReadPermission)
	tx.Create(&employeePositionCreatePermission)
	tx.Create(&employeePositionEditPermission)
	tx.Create(&employeePositionDeletePermission)
	// Create OM Approval Config Permission
	tx.Create(&approvalConfigReadPermission)
	tx.Create(&approvalConfigCreatePermission)
	tx.Create(&approvalConfigEditPermission)
	tx.Create(&approvalConfigDeletePermission)

	// Create PA Employee Permission
	tx.Create(&employeeCreatePermission)
	tx.Create(&employeeReadPermission)
	tx.Create(&employeeEditPermission)
	tx.Create(&profileReadPermission)
	tx.Create(&employeeSearchPermission)

	// Create AA Asset Category Permission
	tx.Create(&assetCategoryReadPermission)
	tx.Create(&assetCategoryCreatePermission)
	tx.Create(&assetCategoryEditPermission)
	tx.Create(&assetCategoryDeletePermission)
	// Create AA Asset Permission
	tx.Create(&assetReadPermission)
	tx.Create(&assetCreatePermission)
	tx.Create(&assetEditPermission)
	tx.Create(&assetDeletePermission)
	// Create AA Asset Operation Permission
	tx.Create(&assetOperationReadPermission)
	tx.Create(&assetOperationCreatePermission)
	tx.Create(&assetOperationEditPermission)

	// Create TMA Holiday Preset Permission
	tx.Create(&holidayPresetReadPermission)
	tx.Create(&holidayPresetCreatePermission)
	tx.Create(&holidayPresetEditPermission)
	tx.Create(&holidayPresetDeletePermission)
	// Create TMA Holiday Company Permission
	tx.Create(&holidayCompanyReadPermission)
	tx.Create(&holidayCompanyCreatePermission)
	tx.Create(&holidayCompanyEditPermission)
	tx.Create(&holidayCompanyDeletePermission)
	// Create TMA Work Preset Permission
	tx.Create(&workPresetReadPermission)
	tx.Create(&workPresetCreatePermission)
	tx.Create(&workPresetEditPermission)
	tx.Create(&workPresetDeletePermission)
	// Create TMA Charge Code Permission
	tx.Create(&chargeCodeReadPermission)
	tx.Create(&chargeCodeCreatePermission)
	tx.Create(&chargeCodeEditPermission)
	tx.Create(&chargeCodeDeletePermission)
	// Create TMA Work Report Permission
	tx.Create(&workReportReadPermission)
	tx.Create(&workReportCreatePermission)
	tx.Create(&workReportEditPermission)
	tx.Create(&workReportDeletePermission)
	// Create TMA Time Sheet Permission
	tx.Create(&timeSheetReadPermission)
	tx.Create(&timeSheetCreatePermission)
	tx.Create(&timeSheetEditPermission)
	// Create TMA Work Adjustment Permission
	tx.Create(&workAdjustmentReadPermission)
	tx.Create(&workAdjustmentCreatePermission)
	tx.Create(&workAdjustmentEditPermission)
	// Create TMA Period Permission
	tx.Create(&ReadPeriodPermission)
	// Create My Work Submission Permission
	tx.Create(&myWorkSubmissionReadPermission)
	// Create Work Submission Permission
	tx.Create(&workSubmissionReadPermission)
	// Create Member Permission
	tx.Create(&memberReadPermission)

	//Create TML Leave Type Permission
	tx.Create(&leaveTypeReadPermission)
	tx.Create(&leaveTypeCreatePermission)
	tx.Create(&leaveTypeEditPermission)
	//Create TML Leave Request Permission
	tx.Create(&leaveRequestReadPermission)
	tx.Create(&leaveRequestCreatePermission)
	tx.Create(&leaveRequestEditPermission)
	//Create TML Leave Report Permission
	tx.Create(&leaveReportReadPermission)
	//Create TML Employee Quota Permission
	tx.Create(&employeeQuotaReadPermission)
	tx.Create(&employeeQuotaEditPermission)
	tx.Create(&employeeQuotaCreatePermission)
	//Create TML Leave Approval Permission
	tx.Create(&leaveApprovalReadPermission)
	tx.Create(&leaveApprovalEditPermission)

	
	// Create Company Request Management Permission
	tx.Create(&registrationRequestReadPermission)
	tx.Create(&registrationRequestEditPermission)
	tx.Create(&terminationRequestCreatePermission)
	tx.Create(&terminationRequestReadPermission)
	tx.Create(&terminationRequestEditPermission)
	tx.Create(&myTerminationRequestEditPermission)

	// Create Account Management Permission
	tx.Create(&accountReadPermission)
	tx.Create(&accountCreatePermission)
	tx.Create(&accountEditPermission)
	tx.Create(&accountDeletePermission)

	// Create My Account Profile Management Permission
	tx.Create(&myAccountProfileReadPermission)
	tx.Create(&PasswordEditPermission)

	// Create Reimbursement Type Management Permission
	tx.Create(&reimbursementTypeReadPermission)
	tx.Create(&reimbursementTypeEditPermission)

	// Create Reimbursement Request Management Permission
	tx.Create(&reimbursementRequestReadPermission)
	tx.Create(&reimbursementRequestCreatePermission)
	tx.Create(&reimbursementRequestEditPermission)

	// Create My Reimbursement Request Management Permission
	tx.Create(&myReimbursementRequestReadPermission)
	tx.Create(&myReimbursementRequestEditPermission)

	// Create Reviewed Reimbursement Request Management Permission
	tx.Create(&reviewedReimbursementRequestReadPermission)
	tx.Create(&reviewedReimbursementRequestEditPermission)

	// Create Approved Reimbursement Request Management Permission
	tx.Create(&approvedReimbursementRequestReadPermission)
	tx.Create(&approvedReimbursementRequestEditPermission)

	// Create Reimbursement Report Permission
	tx.Create(&reimbursementReportEditPermission)

	// Create My Riembursement Perdien Rate
	tx.Create(&myPerdiemRate)

	// Create Role
	tx.Create(&adminRole)
	//means admin has permission  + ref  after create go to migrate 9081 and check DBeaver
	tx.Model(&adminRole).Association("Permission").Append(
		&dashboardReadPermission,
		&userReadPermission,

		// Add OM Company Permission
		&companyReadPermission,
		&companyCreatePermission,
		&companyAddressCreatePermission,
		&companyEditPermission,
		&companyAddressDeletePermission,
		
		// Add TMA Holiday Preset Permission
		&holidayPresetReadPermission,
		&holidayPresetCreatePermission,
		&holidayPresetEditPermission,
		&holidayPresetDeletePermission,

		// Add Company Request Management Permission
		&registrationRequestReadPermission,
		&registrationRequestEditPermission,
		&terminationRequestReadPermission,
		&terminationRequestEditPermission,

		// Create Account Management Permission
		&accountReadPermission,
		&accountCreatePermission,
		&accountEditPermission,
		&accountDeletePermission,
		&PasswordEditPermission,
	)

	tx.Create(&hrRole)
	tx.Model(&hrRole).Association("Permission").Append(
		&dashboardReadPermission,

		// Add PA Employee Permission
		&employeeReadPermission,
		&employeeCreatePermission,
		&employeeEditPermission,
		&employeeSearchPermission,

		// Add OM Company Permission
		&companyAddressCreatePermission,
		&companyEditPermission,
		&companyAddressDeletePermission,
		&myCompanyReadPermission,
		// Add OM Org Level Permission
		&orgLevelReadPermission,
		&orgLevelCreatePermission,
		&orgLevelEditPermission,
		&orgLevelDeletePermission,
		// Add OM Org Unit Permission
		&orgUnitReadPermission,
		&orgUnitCreatePermission,
		&orgUnitEditPermission,
		&orgUnitDeletePermission,
		// Add OM Employee Position Permission
		&employeePositionReadPermission,
		&employeePositionCreatePermission,
		&employeePositionEditPermission,
		&employeePositionDeletePermission,
		// Add OM Approval Config Permission
		&approvalConfigReadPermission,
		&approvalConfigCreatePermission,
		&approvalConfigEditPermission,
		&approvalConfigDeletePermission,

		// Add TMA Holiday Preset Permission
		&holidayPresetReadPermission,
		// Add TMA Holiday Company Permission
		&holidayCompanyReadPermission,
		&holidayCompanyCreatePermission,
		&holidayCompanyEditPermission,
		&holidayCompanyDeletePermission,
		// Add TMA Work Preset Permission
		&workPresetReadPermission,
		&workPresetCreatePermission,
		&workPresetEditPermission,
		&workPresetDeletePermission,
		// Add TMA Charge Code Permission
		&chargeCodeReadPermission,
		&chargeCodeCreatePermission,
		&chargeCodeEditPermission,
		&chargeCodeDeletePermission,
		// Add TMA Work Report Permission
		&workReportReadPermission,
		&workReportCreatePermission,
		&workReportEditPermission,
		&workReportDeletePermission,
		// Add TMA Period Permission
		&ReadPeriodPermission,
		// Add TMA Work Submission Permission
		&myWorkSubmissionReadPermission,
		// Add TMA Time Sheet Permission
		&timeSheetReadPermission,
		&timeSheetCreatePermission,
		&timeSheetEditPermission,
		// Add TMA Work Adjustment Permission
		&workAdjustmentReadPermission,
		&workAdjustmentCreatePermission,
		&workAdjustmentEditPermission,
		// Add TML Leave Type Permission
		&leaveTypeReadPermission,
		&leaveTypeCreatePermission,
		&leaveTypeEditPermission,
		// Add TML Leave Request Permission
		&leaveRequestReadPermission,
		&leaveRequestCreatePermission,
		&leaveRequestEditPermission,
		// Add TML Leave Report Permission
		&leaveReportReadPermission,
		// Add TML Employee Quota Permission
		&employeeQuotaReadPermission,
		&employeeQuotaEditPermission,
		&employeeQuotaCreatePermission,
		// Add TML Leave Approval Permission
		&leaveApprovalReadPermission,
		&leaveApprovalEditPermission,

		// Add Account Management Permission
		&accountEditPermission,

		// Add My Account Profile Management Permission
		&myAccountProfileReadPermission,
		&PasswordEditPermission,

		// Add Reimbursement Type Management Permission
		&reimbursementTypeReadPermission,
		&reimbursementTypeEditPermission,

		// Add Reimbursement Request Management Permission
		&reimbursementRequestReadPermission,
		&reimbursementRequestCreatePermission,
		&reimbursementRequestEditPermission,

		// Add My Reimbursement Request Management Permission
		&myReimbursementRequestReadPermission,
		&myReimbursementRequestEditPermission,

		// Add Reviewed Reimbursement Request Management Permission
		&reviewedReimbursementRequestReadPermission,
		&reviewedReimbursementRequestEditPermission,

		// Add My Riembursement Peridem Rate
		&myPerdiemRate,
	)

	tx.Create(&employeeRole)
	tx.Model(&employeeRole).Association("Permission").Append(
		&dashboardReadPermission,

		// PA Employee Permission
		&profileReadPermission,
		&employeeSearchPermission,

		// Add TMA Holiday Company Permission
		&holidayCompanyReadPermission,
		// Add TMA Work Preset Permission
		&workPresetReadPermission,
		// Add TMA Charge Code Permission
		&chargeCodeReadPermission,
		// Add TMA Time Sheet Permission
		&timeSheetReadPermission,
		&timeSheetCreatePermission,
		&timeSheetEditPermission,
		// Add TMA Work Adjustment Permission
		&workAdjustmentReadPermission,
		&workAdjustmentCreatePermission,
		&workAdjustmentEditPermission,
		// Add TMA Period Permission
		&ReadPeriodPermission,
		// Add TMA My Work Submission Permission
		&myWorkSubmissionReadPermission,

		// Add TML Leave Type Permission
		&leaveTypeReadPermission,
		// Add TML Leave Request Permission
		&leaveRequestReadPermission,
		&leaveRequestCreatePermission,
		&leaveRequestEditPermission,
		// Add TML Employee Quota Permission
		&employeeQuotaReadPermission,

		// Add My Account Profile Management Permission
		&myAccountProfileReadPermission,
		&PasswordEditPermission,

		// Add My Reimbursement Request Management Permission
		&myReimbursementRequestReadPermission,
		&myReimbursementRequestEditPermission,

		// Add Reimbursement Request Management Permission
		&reimbursementRequestCreatePermission,
		
		// Add My Riembursement Peridem Rate
		&myPerdiemRate,
	)

	tx.Create(&backOfficeAdminRole)
	tx.Model(&backOfficeAdminRole).Association("Permission").Append(
		&dashboardReadPermission,
		
		// Add OM Company Permission
		&companyEditPermission,
		&companyAddressCreatePermission,
		&companyAddressDeletePermission,
		&myCompanyReadPermission,
		// Add AA Asset Category Permission
		&assetCategoryReadPermission,
		&assetCategoryCreatePermission,
		&assetCategoryEditPermission,
		&assetCategoryDeletePermission,
		// Add AA Asset Permission
		&assetReadPermission,
		&assetCreatePermission,
		&assetEditPermission,
		&assetDeletePermission,
		// Add AA Asset Operation Permission
		&assetOperationReadPermission,
		&assetOperationCreatePermission,
		&assetOperationEditPermission,

		// Add TML Leave Apporval Permission
		&leaveApprovalReadPermission,
		&leaveApprovalEditPermission,
		// Add TML Leave Type Permission
		&leaveTypeReadPermission,
		// Add TML Employee Quota Permission
		&employeeQuotaReadPermission,
		// Add TML Leave Request Permission
		&leaveRequestReadPermission,
		&leaveRequestCreatePermission,
		&leaveRequestEditPermission,

		// Add TMA Member Permission
		&memberReadPermission,
		// Add TMA Work Preset Permission
		&workPresetReadPermission,
		&workPresetCreatePermission,
		&workPresetEditPermission,
		&workPresetDeletePermission,
		// Add TMA Work Adjustment Permission
		&workAdjustmentReadPermission,
		&workAdjustmentCreatePermission,
		&workAdjustmentEditPermission,
		// Add TMA Period Permission
		&ReadPeriodPermission,
		// Add TMA Charge Code Permission
		&chargeCodeReadPermission,
		&chargeCodeCreatePermission,
		&chargeCodeEditPermission,
		&chargeCodeDeletePermission,
		// Add TMA Work Report Permission
		&workReportReadPermission,
		&workReportCreatePermission,
		&workReportEditPermission,
		&workReportDeletePermission,
		// Add TMA Time Sheet Permission
		&timeSheetReadPermission,
		&timeSheetCreatePermission,
		&timeSheetEditPermission,
		// Add TMA My Work Submission Permission
		&myWorkSubmissionReadPermission,
		
		// Add PA Employee Permission
		&employeeSearchPermission,
		&employeeReadPermission,
		&profileReadPermission,

		// Create Account Management Permission
		&accountReadPermission,
		&accountCreatePermission,
		&accountEditPermission,
		&accountDeletePermission,
		
		// Add My Account Profile Management Permission
		&myAccountProfileReadPermission,
		&PasswordEditPermission,

		// Add Company Request Management Permission
		&terminationRequestCreatePermission,
		&myTerminationRequestEditPermission,

		// Add Reimbursement Request Management Permission
		&reimbursementRequestReadPermission,
		&reimbursementRequestCreatePermission,
		&reimbursementRequestEditPermission,

		// Add My Reimbursement Request Management Permission
		&myReimbursementRequestReadPermission,
		&myReimbursementRequestEditPermission,

		// Add Approved Reimbursement Request Management Permission
		&approvedReimbursementRequestReadPermission,
		&approvedReimbursementRequestEditPermission,

		// Add Reimbursement Report Permission
		&reimbursementReportEditPermission,
		
		// Add My Riembursement Peridem Rate
		&myPerdiemRate,
	)

	tx.Create(&managerRole)
	tx.Model(&managerRole).Association("Permission").Append(
		
		// Add TML Leave Type Permission
		&leaveTypeReadPermission,
		// Add TML Leave Request Permission
		&leaveRequestReadPermission,
		&leaveRequestCreatePermission,
		&leaveRequestEditPermission,
		// Add TML Employee Quota Permission
		&employeeQuotaReadPermission,
		// Add TML Leave Apporval Permission
		&leaveApprovalReadPermission,
		&leaveApprovalEditPermission,

		// Add TMA Holiday Company Permission
		&holidayCompanyReadPermission,
		// Add TMA Work Preset Permission
		&workPresetReadPermission,
		// Add TMA Charge Code Permission
		&chargeCodeReadPermission,
		&chargeCodeEditPermission,
		&chargeCodeCreatePermission,
		// Add TMA Work Report Permission
		&workReportReadPermission,
		&workReportCreatePermission,
		&workReportEditPermission,
		&workReportDeletePermission,
		// Add TMA Time Sheet Permission
		&timeSheetReadPermission,
		&timeSheetCreatePermission,
		&timeSheetEditPermission,
		// Add TMA Work Adjustment Permission
		&workAdjustmentReadPermission,
		&workAdjustmentCreatePermission,
		&workAdjustmentEditPermission,
		// Add TMA Period Permission
		&ReadPeriodPermission,
		// Add TMA Work Submission Permission
		&workSubmissionReadPermission,
		// Add TMA My Work Submission Permission
		&myWorkSubmissionReadPermission,

		// Add PA Employee Permission
		&employeeSearchPermission,
		&employeeReadPermission,
		&profileReadPermission,

		// Add My Account Profile Management Permission
		&myAccountProfileReadPermission,
		&PasswordEditPermission,

		// Add Reimbursement Request Management Permission
		&reimbursementRequestReadPermission,
		&reimbursementRequestCreatePermission,
		&reimbursementRequestEditPermission,
		
		// Add My Reimbursement Request Management Permission
		&myReimbursementRequestReadPermission,
		&myReimbursementRequestEditPermission,

		// Add Reviewed Reimbursement Request Management Permission
		&reviewedReimbursementRequestReadPermission,
		&reviewedReimbursementRequestEditPermission,

		//DashBoard
		&dashboardReadPermission,
		
		// Add My Riembursement Peridem Rate
		&myPerdiemRate,
	)

	// Create Password Session
	tx.Create(&passwordSeesion1)
	tx.Create(&passwordSeesion2)
	tx.Create(&passwordSeesion3)
	tx.Create(&passwordSeesion4)
	tx.Create(&passwordSeesion5)
	tx.Create(&passwordSeesion6)
	tx.Create(&passwordSeesion7)
	tx.Create(&passwordSeesion8)
	tx.Create(&passwordSeesion9)
	tx.Create(&passwordSeesion10)
	tx.Create(&passwordSeesion11)
	tx.Create(&passwordSeesion12)
	tx.Create(&passwordSeesion13)
	tx.Create(&passwordSeesion14)
	tx.Create(&passwordSeesion15)
	tx.Create(&passwordSeesion16)
	tx.Create(&passwordSeesion17)
	tx.Create(&passwordSeesion18)
	tx.Create(&passwordSeesion19)
	tx.Create(&passwordSeesion20)
	tx.Create(&passwordSeesion21)

	tx.Create(&model.User{
		ID:       1,
		Username: "jeurboy",
		Password: "$2a$04$4PGXZWV4OxwtMkU.fl3RMus6KTeamOJfJjSLycqHwjeoVeArDtdJC", // boy
		Role:     &adminRole,
	})

	//? Mock User for login
	tx.Create(&model.User{
		Username: "thanchanok_phr@dv.co.th",
		Password: "$2a$04$4PGXZWV4OxwtMkU.fl3RMus6KTeamOJfJjSLycqHwjeoVeArDtdJC", // boy
		Role:     &hrRole,
	})

	//? Mock User for login
	tx.Create(&model.User{
		Username: "ratchapoom_rat@dv.co.th",
		Password: "$2a$04$4PGXZWV4OxwtMkU.fl3RMus6KTeamOJfJjSLycqHwjeoVeArDtdJC", // boy
		Role:     &backOfficeAdminRole,
	})

	//? Mock User for login
	tx.Create(&model.User{
		Username: "siripan_sae@dv.co.th",
		Password: "$2a$04$4PGXZWV4OxwtMkU.fl3RMus6KTeamOJfJjSLycqHwjeoVeArDtdJC", // boy
		Role:     &managerRole,
	})
	//? Mock User for login
	tx.Create(&model.User{
		Username: "kattareya_suw@dv.co.th",
		Password: "$2a$04$4PGXZWV4OxwtMkU.fl3RMus6KTeamOJfJjSLycqHwjeoVeArDtdJC", // boy
		Role:     &managerRole,
	})
	//? Mock User for login
	tx.Create(&model.User{
		Username: "chonnabot_dun@dv.co.th",
		Password: "$2a$04$4PGXZWV4OxwtMkU.fl3RMus6KTeamOJfJjSLycqHwjeoVeArDtdJC", // boy
		Role:     &managerRole,
	})
	//? Mock User for login
	tx.Create(&model.User{
		Username: "phatya_cha@dv.co.th",
		Password: "$2a$04$4PGXZWV4OxwtMkU.fl3RMus6KTeamOJfJjSLycqHwjeoVeArDtdJC", // boy
		Role:     &managerRole,
	})
	//? Mock User for login
	tx.Create(&model.User{
		Username: "kunlapat_pun@dv.co.th",
		Password: "$2a$04$4PGXZWV4OxwtMkU.fl3RMus6KTeamOJfJjSLycqHwjeoVeArDtdJC", // boy
		Role:     &managerRole,
	})
	//? Mock User for login
	tx.Create(&model.User{
		Username: "chanapa_sir@dv.co.th",
		Password: "$2a$04$4PGXZWV4OxwtMkU.fl3RMus6KTeamOJfJjSLycqHwjeoVeArDtdJC", // boy
		Role:     &managerRole,
	})
	//? Mock User for login
	tx.Create(&model.User{
		Username: "krittayanee_thu@dv.co.th",
		Password: "$2a$04$4PGXZWV4OxwtMkU.fl3RMus6KTeamOJfJjSLycqHwjeoVeArDtdJC", // boy
		Role:     &employeeRole,
	})
	//? Mock User for login
	tx.Create(&model.User{
		Username: "thanakan_tha@dv.co.th",
		Password: "$2a$04$4PGXZWV4OxwtMkU.fl3RMus6KTeamOJfJjSLycqHwjeoVeArDtdJC", // boy
		Role:     &employeeRole,
	})
	//? Mock User for login
	tx.Create(&model.User{
		Username: "Thanakan_tha2@dv.co.th",
		Password: "$2a$04$4PGXZWV4OxwtMkU.fl3RMus6KTeamOJfJjSLycqHwjeoVeArDtdJC", // boy
		Role:     &backOfficeAdminRole,
	})
	//? Mock User for login
	tx.Create(&model.User{
		Username: "Thanakan_tha3@dv.co.th",
		Password: "$2a$04$4PGXZWV4OxwtMkU.fl3RMus6KTeamOJfJjSLycqHwjeoVeArDtdJC", // boy
		Role:     &backOfficeAdminRole,
	})
	//? Mock User for login
	tx.Create(&model.User{
		Username: "Thanakan_tha4@dv.co.th",
		Password: "$2a$04$4PGXZWV4OxwtMkU.fl3RMus6KTeamOJfJjSLycqHwjeoVeArDtdJC", // boy
		Role:     &backOfficeAdminRole,
	})
	//? Mock User for login
	tx.Create(&model.User{
		Username: "Thanakan_tha5@dv.co.th",
		Password: "$2a$04$4PGXZWV4OxwtMkU.fl3RMus6KTeamOJfJjSLycqHwjeoVeArDtdJC", // boy
		Role:     &backOfficeAdminRole,
	})
	//? Mock User for login
	tx.Create(&model.User{
		Username: "Thanakan_tha6@dv.co.th",
		Password: "$2a$04$4PGXZWV4OxwtMkU.fl3RMus6KTeamOJfJjSLycqHwjeoVeArDtdJC", // boy
		Role:     &backOfficeAdminRole,
	})
	//? Mock User for login
	tx.Create(&model.User{
		Username: "Thanakan_tha7@dv.co.th",
		Password: "$2a$04$4PGXZWV4OxwtMkU.fl3RMus6KTeamOJfJjSLycqHwjeoVeArDtdJC", // boy
		Role:     &backOfficeAdminRole,
	})
	//? Mock User for login
	tx.Create(&model.User{
		Username: "Thanakan_tha8@dv.co.th",
		Password: "$2a$04$4PGXZWV4OxwtMkU.fl3RMus6KTeamOJfJjSLycqHwjeoVeArDtdJC", // boy
		Role:     &backOfficeAdminRole,
	})
	//? Mock User for login
	tx.Create(&model.User{
		Username: "Thanakan_tha9@dv.co.th",
		Password: "$2a$04$4PGXZWV4OxwtMkU.fl3RMus6KTeamOJfJjSLycqHwjeoVeArDtdJC", // boy
		Role:     &backOfficeAdminRole,
	})
	//? Mock User for login
	tx.Create(&model.User{
		Username: "Thanakan_tha10@dv.co.th",
		Password: "$2a$04$4PGXZWV4OxwtMkU.fl3RMus6KTeamOJfJjSLycqHwjeoVeArDtdJC", // boy
		Role:     &backOfficeAdminRole,
	})
	//? Mock User for login
	tx.Create(&model.User{
		Username: "dream@dv.co.th",
		Password: "$2a$04$4PGXZWV4OxwtMkU.fl3RMus6KTeamOJfJjSLycqHwjeoVeArDtdJC", // boy
		Role:     &employeeRole,
	})
	//? Mock User for login
	tx.Create(&model.User{
		Username: "kasalong_thu@dv.co.th",
		Password: "$2a$04$4PGXZWV4OxwtMkU.fl3RMus6KTeamOJfJjSLycqHwjeoVeArDtdJC", // boy
		Role:     &employeeRole,
	})

	return tx.Commit().Error
}

func Rollback_201908142036(tx *gorm.DB) error {
	return tx.Rollback().Error
}

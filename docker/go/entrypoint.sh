#!/bin/bash

# git config --global url."git@bitbucket.org:".insteadOf "https://bitbucket.org/"
# chmod 0600 /root/.ssh
# chmod 0600 /root/.ssh/*
# ssh -o StrictHostKeyChecking=no bitbucket.org

cd /go/src/api
go mod tidy

echo "✅ Install Go Mod Download"
go build -mod vendor

echo "✅ Start service ..."
realize start --run